// fichier principal, qui définir les variables globales (game et ctx) et instancier la partie + définir la fenêtre de canvas

import { Engine } from "./src/core/Engine"
import './styles.scss'

const engine = new Engine()

const canvas = <HTMLCanvasElement> document.querySelector("#game")
const ctx = canvas.getContext("2d")!

ctx.canvas.width = window.innerWidth
ctx.canvas.height = window.innerHeight

globalThis.engine = engine
globalThis.ctx = ctx

// on démarre le jeu
engine.loadScene('menu')