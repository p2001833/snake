import { Snake, Apple, Wall } from '../entities'

import gameConfig from '../../config/game.json'
import levelsConfig from '../../config/levels.json'

import { getCanvasPosition, isPositionInElement } from '../utils/position'
import { getRandomDirection } from '../utils/random'
import { playSound } from '../utils/sound'

export class Game {

    private _snake = new Snake({x: 0, y: 0})
    private _apple = new Apple({x: 0, y: 0})
    private _walls: Wall[] = []

    // setup

    awake() {

        // on initialise les évènements
        this.setEvents()
        
        // on initialise le niveau
        if (engine.state.level > 0) {
            this.loadLevel(engine.state.level)
        } 
        // mode infini
        else if (engine.state.level === -1) { 

            // position et direction du serpent aléatoire
            this.snake.position = { x: Math.floor(Math.random() * engine.world.size.grid.width), y: Math.floor(Math.random() * engine.world.size.grid.height) }
            this.snake.velocity = getRandomDirection()
            engine.state.speedMultiplier = 1 - gameConfig.defaultSpeedMultiplier

            this.generateLevel()
        }

        engine.state.score = 0
        engine.state.lastKeyPressed = Date.now()

        // on lance la boucle de jeu une première fois
        this.loop()
    }

    setEvents() {

        this.onKeyDown = this.onKeyDown.bind(this)
        document.querySelector('body')!.addEventListener("keydown", this.onKeyDown, true)
    }

    destroy() { 

        document.querySelector('body')!.removeEventListener("keydown", this.onKeyDown, true)
    }

    // boucle qui va contenir la logique du jeu

    loop() {

        // on ne fait rien si la pause est activée
        if (!engine.state.isPaused) {

            this.snake.move()
            this.handleCollisions()
            this.snake.adaptTrail()
        }

        this.drawAll()

        // cette fonction (loop) va être réappelée dans un intervalle de temps défini par la fonction getSpeed()
        if (engine.scene === this) setTimeout(() => this.loop(), this.getSpeed())
    }


    // ===========================================
    // ==========  gestion des niveaux  ==========
    // ===========================================


    loadLevel(level: number) {

        const levelConfig = levelsConfig[level - 1]

        engine.world.size.grid.width = levelConfig.dimensions[0]
        engine.world.size.grid.height = levelConfig.dimensions[1]

        this.walls = []

        for (const wall of levelConfig.walls) {
            this.walls.push(new Wall({x: wall[0], y: wall[1]}))
        }

        this.snake.position = { x: levelConfig.snake[0], y: levelConfig.snake[1] }
        this.snake.velocity = getRandomDirection()

        this.newApple()

        engine.state.level = level
        engine.state.score = 1
        engine.state.speedMultiplier = levelConfig.speedMultiplier
        engine.state.numberOfRounds = levelConfig.numberOfRounds
    }

    // fonction qui permet soit de passer au niveau suivant, soit de générer un nouveau niveau si on est en mode Infini
    nextLevel() {

        // réinitialisation de la queue du serpent
        this.snake.trail = []

        if (engine.state.level === -1) {

            // on génère un nouveau niveau
            this.generateLevel()

        } 
        else if (engine.state.level > 0) {
            
            // on vérifie si on est au niveau maximum
            if (engine.state.level === levelsConfig.length) {
                // si c'est le cas, on revient au menu
                engine.loadScene('menu')
            } else {
                // sinon, on passe au niveau suivant 
                this.loadLevel(engine.state.level + 1)
            }
        }
    }

    private generateLevel() {

        // on re-crée une nouvelle pomme
        this.newApple()

        // augmentation de la vitesse de la partie
        engine.state.speedMultiplier += gameConfig.defaultSpeedMultiplier
    }


    // ===========================================
    // ========== fonctions utilitaires ==========
    // ===========================================

    // renvoie la vitesse de la boucle de jeu en fonction du niveau actuel
    private getSpeed() {
        return gameConfig.baseSpeed / engine.state.speedMultiplier
    }

    // crée une nouvelle pomme avec une position aléatoire, en faisant attention à ce que cette dernière ne soit pas dans le serpent ou dans un mur
    private newApple() {
        this.apple = new Apple(Apple.getRandomApplePosition(this.snake.trail, this.walls))
    }

    // nous gérons ici toutes les collisions du jeu
    handleCollisions() {

        // le serpent touche un bord de la map (on le fait ressortir de l'autre côté de la map)
        if (this.snake.position.x < 0)                                 this.snake.position.x = engine.world.size.grid.width - 1
        if (this.snake.position.x > engine.world.size.grid.width - 1)  this.snake.position.x = 0
        if (this.snake.position.y < 0)                                 this.snake.position.y = engine.world.size.grid.height - 1
        if (this.snake.position.y > engine.world.size.grid.height - 1) this.snake.position.y = 0

        // le serpent touche sa queue
        if (isPositionInElement(this.snake.position, this.snake.trail)) this.gameOver()

        // le serpent touche un mur
        for (const wall of this.walls) {
            if (wall.position.x === this.snake.position.x && wall.position.y === this.snake.position.y) {
                this.gameOver()
            }
        }

        // le serpent touche une pomme
        if (this.snake.position.x === this.apple.position.x && this.snake.position.y === this.apple.position.y) {

            playSound('low.mp3')
            // si le nombre de round est égal au nombre de round total, on passe au niveau suivant
            if (engine.state.score === engine.state.numberOfRounds) {
                this.nextLevel()
            } else { // sinon, on fait grandir le serpent et on génère un nouveau niveau
                engine.state.score++
                this.snake.grow()
                this.generateLevel()
            }
        }        
    }

    // fonction appellée lors d'un game over
    gameOver() {
        playSound('die.mp3')
        engine.loadScene('menu')
    }

    // ===========================================
    // ==========        canvas         ==========
    // ===========================================

    drawAll() {

        // on efface le canvas pour le redessiner
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)

        // on dessine toutes les composantes du jeu
        this.drawBackground()
        this.drawScore()
        this.snake.draw()
        this.apple.draw()
        for (const wall of this.walls) wall.draw()

        if (engine.state.isPaused) {
            this.drawPause()
        }
    }

    private drawPause() {

        ctx.fillStyle = 'rgba(0, 0, 0, 0.7)'
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)

        ctx.fillStyle = 'white'
        ctx.font = '100px Poppins'
        ctx.textAlign = 'center'
        ctx.fillText('PAUSE', ctx.canvas.width / 2, ctx.canvas.height / 2)
    }

    private drawBackground() {

        const position = getCanvasPosition({x: 0, y: 0}),
              mapSize = engine.getMapSize()
        
        ctx.fillStyle = "#9BBA5A"
        ctx.strokeStyle = "#272F17"
        ctx.lineWidth = 10

        ctx.rect(position.x, position.y, mapSize.width, mapSize.height)

        ctx.stroke()
        ctx.fill()
    }

    drawScore() {

        const position = getCanvasPosition({x: 0, y: 0}),
              mapSize = engine.getMapSize()

        ctx.fillStyle = "#829c4e"
        
        if (engine.state.level === -1) {


            ctx.font = "100px Kumbh Sans"
    
            const text = engine.state.score
    
            ctx.fillText(
                text,
                position.x + mapSize.width / 2 - ctx.measureText(text).width / 2,
                position.y + mapSize.height / 2 + 20
            )
            
        } else {

            ctx.font = "60px Kumbh Sans"
        
            ctx.fillText(
                'Niveau ' + engine.state.level,
                position.x + mapSize.width / 2,
                position.y + mapSize.height / 2 - 30
            )

            ctx.font = "30px Kumbh Sans"

            ctx.fillText(
                'Round ' + engine.state.score,
                position.x + mapSize.width / 2,
                position.y + mapSize.height / 2 + 30
            )
        }
    }

    // ===========================================
    // ==========       events          ==========
    // ===========================================

    onKeyDown(event: KeyboardEvent) {

        // activer/désactiver le mode pause
        if (event.key === ' ') {
            engine.state.isPaused = !engine.state.isPaused
        }
        else if (event.key === 'Escape') {
            this.gameOver()
        }
        
        // on ne fait rien si le jeu est en pause
        if (!engine.state.isPaused && Date.now() - engine.state.lastKeyPressed > 75) {

            // on change la vélocité en fonction de la flèche pressée
            if (event.key === 'ArrowUp' && this.snake.velocity.y !== 1) {
                this.snake.velocity.x = 0
                this.snake.velocity.y = -1
            }
            else if (event.key === 'ArrowDown' && this.snake.velocity.y !== -1) {
                this.snake.velocity.x = 0
                this.snake.velocity.y = 1
            }
            else if (event.key === 'ArrowLeft' && this.snake.velocity.x !== 1) {
                this.snake.velocity.x = -1
                this.snake.velocity.y = 0
            }
            else if (event.key === 'ArrowRight' && this.snake.velocity.x !== -1) {
                this.snake.velocity.x = 1
                this.snake.velocity.y = 0
            }

            engine.state.lastKeyPressed = Date.now()
        }
    }

    // ===========================================
    // ==========  getters et setters   ==========
    // ===========================================

    get snake () { return this._snake }
    
    get apple () { return this._apple }
    set apple (apple: Apple) { this._apple = apple }

    get walls () { return this._walls }
    set walls (walls: Wall[]) { this._walls = walls }
}