import levels from '../../config/levels.json'
import { playSound } from '../utils/sound'

export class Menu {

    private readonly levels = levels
        .map((_, i) => { return { displayName: `Niveau ${i+1}`, value: i + 1 } })
        .concat(
            { displayName: 'Infini', value: -1 }
        )
    
    awake () {

        // niveau par défaut
        engine.state.level = this.levels[0]
        
        // on initialise les évènements
        this.setEvents()

        // on lance la boucle de jeu une première fois
        this.loop()
    }

    setEvents() {

        this.onKeyDown = this.onKeyDown.bind(this)
        document.querySelector('body')!.addEventListener("keydown", this.onKeyDown, true)
    }

    destroy() { 

        document.querySelector('body')!.removeEventListener("keydown", this.onKeyDown, true)
    }

    loop() {

        this.drawAll()

        // cette fonction (loop) va être réappelée dans un intervalle de temps (3x par secondes)
        if (engine.scene === this) setTimeout(() => this.loop(), 1000 / 3)
    }

    drawAll() {

        // on efface l'écran
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)

        // on dessine le fond
        ctx.fillStyle = '#9BBA5A'
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)

        // on dessine le texte
        ctx.fillStyle = '#272F17'
        ctx.font = '150px Black And White Picture'
        ctx.fillText('Snake', ctx.canvas.width / 2, ctx.canvas.height / 2 - 200)

        ctx.fillStyle = '#272F17'
        ctx.font = '30px Arial'
        ctx.textAlign = 'center'
        ctx.fillText('Choisi ton niveau !', ctx.canvas.width / 2, ctx.canvas.height / 2 - 50)

        ctx.font = '15px Arial'
        ctx.fillText('(Appuie sur ↑ ou ↓ pour changer de niveau)', ctx.canvas.width / 2, ctx.canvas.height / 2 - 10)

        ctx.font = '30px Archivo Black'
        ctx.fillText(engine.state.level.displayName, ctx.canvas.width / 2, ctx.canvas.height / 2 + 50)

        ctx.font = '20px Arial'
        ctx.fillText('Appuie sur ESPACE pour commencer', ctx.canvas.width / 2, ctx.canvas.height / 2 + 150)
    }


    // ===========================================
    // ==========       events          ==========
    // ===========================================

    onKeyDown(event: KeyboardEvent) {

        // on change le niveau sélectionné en fonction de la flèche pressée
        if (event.key === 'ArrowDown') {
            engine.state.level = this.prevLevel()
        }        
        else if (event.key === 'ArrowUp') {
            engine.state.level = this.nextLevel()
        }
        else if (event.key === ' ') {
            // on lance la partie
            engine.state.level = engine.state.level.value
            engine.state.isPaused = false
            engine.loadScene('game')
        }
    }

    private prevLevel() {

        playSound('high.mp3')
        const newIndex = this.levels.indexOf(engine.state.level) - 1
        return this.levels[newIndex < 0 ? this.levels.length - 1 : newIndex]
    }

    private nextLevel() {

        playSound('high.mp3')
        const newIndex = this.levels.indexOf(engine.state.level) + 1
        return this.levels[newIndex >= this.levels.length ? 0 : newIndex]
    }
}