import worldConfig from '../../config/world.json'
import { Game, Menu} from '../scenes'

export class Engine {

    private _world = worldConfig
    private _state: any = {}
    private _currentScene: Menu | Game | null = null

    awake() {

        this.loadScene('menu')
    
        this.loop()
    }

    
    loop() {
        this._currentScene!.loop()
    }
    
    loadScene(sceneName: string) {

        // si une scène existe, on la détruit avant de charger la suivante
        if (this._currentScene) this._currentScene.destroy()
        
        // on charge la nouvelle scène
        if (sceneName === 'menu') this._currentScene = new Menu()
        else if (sceneName === 'game') this._currentScene = new Game()

        // on lance la scène
        this._currentScene!.awake()
    }

    getMapSize(): Size {

        return {
            width: engine.world.size.grid.width * engine.world.size.case,
            height: engine.world.size.grid.height * engine.world.size.case
        }
    }

    // ===========================================
    // ==========  getters et setters   ==========
    // ===========================================

    get scene() { return this._currentScene }

    get state() { return this._state }
    set state(state) { this._state = state }
    
    get world() { return this._world }
}