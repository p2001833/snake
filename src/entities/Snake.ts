import { getCanvasPosition } from '../utils/position'

export class Snake {

    private _position: Vector
    private _velocity: Vector
    private _trail: Vector[] = []
    private _tail: number = 5

    constructor(position: Vector) {
        this._position = position
        this._velocity = { x: 0, y: 0 }
    }

    move() {

        // on calcule la nouvelle position du serpent à partir de sa vélocité
        this.position.x += this.velocity.x
        this.position.y += this.velocity.y
    }

    // cette fonction permet de garder en mémoire les positions de la queue du serpent
    adaptTrail() {

        // on ajoute la dernière position du serpent à la fin du tableau
        this.trail.push({
            x: this.position.x,
            y: this.position.y
        })

        // tant que le tableau est plus grand que la taille du serpent, on supprime le premier élément
        while (this.trail.length > this.tail) {
            this.trail.shift()
        }
    }
    
    grow() {
        this.tail += 3
    }

    // ===========================================
    // ==========        canvas         ==========
    // ===========================================

    draw() {

        ctx.fillStyle = "lime"

        for (const trailElement of this.trail) {
            
            const scaledPosition = getCanvasPosition(trailElement)
            
            ctx.beginPath()

            ctx.arc(
                scaledPosition.x + engine.world.size.case / 2,
                scaledPosition.y + engine.world.size.case / 2,
                engine.world.size.case / 2,
                0,
                2 * Math.PI
            )

            ctx.fill()
            ctx.closePath()
        }
    }

    // ===========================================
    // ==========  getters et setters   ==========
    // ===========================================

    get position() { return this._position }
    set position(position: Vector) { this._position = position }

    get velocity() { return this._velocity }
    set velocity(velocity: Vector) { this._velocity = velocity }
    
    get trail() { return this._trail }
    set trail(trail: Vector[]) { this._trail = trail }

    get tail() { return this._tail }
    set tail(tailSize: number) { this._tail = tailSize }

}