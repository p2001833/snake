import { getCanvasPosition } from "../utils/position"

export class Wall {

    private readonly _position: Vector

    constructor(position: Vector) {
        this._position = position
    }

    // ===========================================
    // ==========        canvas         ==========
    // ===========================================

    draw() {

        ctx.fillStyle = "brown"

        const scaledPosition = getCanvasPosition(this.position)

        ctx.fillRect(
            scaledPosition.x, 
            scaledPosition.y, 
            engine.world.size.case - 2, 
            engine.world.size.case - 2
        )
    }

    // ===========================================
    // ==========  getters et setters   ==========
    // ===========================================

    get position(): Vector { return this._position }
}