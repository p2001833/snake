import { isPositionInElement, getCanvasPosition } from "../utils/position"
import { Wall } from "./Wall"

export class Apple {

    private readonly _position: Vector;

    constructor(position: Vector) {
        this._position = position
    }

    // ===========================================
    // ==========        canvas         ==========
    // ===========================================

    draw() {

        ctx.fillStyle = "red"

        const scaledPosition = getCanvasPosition(this.position)

        ctx.fillRect(
            scaledPosition.x, 
            scaledPosition.y, 
            engine.world.size.case, 
            engine.world.size.case
        )
    }

    // ===========================================
    // ==========  getters et setters   ==========
    // ===========================================

    get position() { return this._position }

    // ===========================================
    // ==========  fonctions statiques  ==========
    // ===========================================

    static getRandomApplePosition(snakeTrail: Vector[], walls: Wall[]) {

        let randomPosition: Vector

        // on génère une position aléatoire tant que cette dernière n'est pas dans le serpent ou dans un mur
        do {
            randomPosition = {
                x: Math.floor(Math.random() * engine.world.size.grid.width),
                y: Math.floor(Math.random() * engine.world.size.grid.height)
            }
        } while (isPositionInElement(randomPosition, snakeTrail) || isPositionInElement(randomPosition, walls.map((wall: Wall) => wall.position)))
        
        return randomPosition
    }
}