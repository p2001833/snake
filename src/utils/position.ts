export function isPositionInElement(position: Vector, elements: Vector[]): boolean {
            
    for (const element of elements) {
        if (position.x === element.x && position.y === element.y) {
            return true
        }
    }
    return false
}


export function getCenteredPosition(position: Vector): Vector {

    const mapSize = engine.getMapSize(),
          bottomMargin = 20

    return {
        x: position.x + (ctx.canvas.width - mapSize.width) / 2,
        y: (position.y + (ctx.canvas.height - mapSize.height) / 2) + bottomMargin
    }
}


export function getScaledPosition(position: Vector): Vector {
    
    return {
        x: position.x * engine.world.size.case,
        y: position.y * engine.world.size.case
    }
}


export function getCanvasPosition(position: Vector) {

    return getCenteredPosition(getScaledPosition(position))
}