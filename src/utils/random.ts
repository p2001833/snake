export function getRandomDirection(): Vector {
    
    const directions = [[1, 0], [0, 1], [-1, 0], [0, -1]],
          direction = directions[Math.floor(Math.random() * directions.length)]

    return {
        x: direction[0],
        y: direction[1]
    }
}