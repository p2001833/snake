export function playSound(fileName: string) {
    const audio = new Audio('assets/' + fileName)
    audio.play()
}