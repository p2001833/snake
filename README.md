# Snake

Jeu du **Snake** réalisé dans le cadre du projet final du module `Web Client Riche` de l'IUT Lyon 1 (Doua).
Membres du projet :
- **Bartholomé GILI** (G1S4)
- **Lazare CHEVEREAU** (G1S4)

## Choix techniques

### TypeScript

Le projet a été réalisé en utilisant le langage de programmation `TypeScript`.
Ce langage est une surcouche du *Javascript* qui permet principalement d'y ajouter la notion de **types**.
Nous avons décidé de le coder dans ce langage car nous y sommes déjà familiés, le matrisons bien et que le langage ressemble trait pour trait au *JavaScript*, ce qui ne vous posera pas beaucoup plus de problème pour la correction.

### Vite.js

Afin de bundler, nous avons utilisé la librairie `Vite.js`, très efficace et simple d'utilisation. Il permet nottament de faciliter les imports et de compiler le projet (pour rappel, TypeScript est une surcouche qui compile vers JavaScript).

## Pré-requis

- `Node.js`

## Installation

1. `git clone https://forge.univ-lyon1.fr/p2001833/snake.git`
2. `cd snake`
3. `npm install`
4. `npm run start`
5. Rendez-vous à l'adresse `http://localhost:3000`

> Si jamais vous n'arrivez pas à le faire tourner en local, le site est accessible ici : [https://snake.barthofu.com](https://snake.barthofu.com)

## Utilisation

Le jeu est composé de deux "interfaces" (appelées `scenes` au sein du code) :
- Le **Menu**, depuis lequel vous pourrez choisir le niveau à l'aide des flèches `haut` et `bas`, et commencer la partie avec `espace`.
- Le **Jeu**, dans lequel vous pouvez vous déplacer avec la `flèche directionnelle`, mettre en pause la partie avec `espace` et enfin revenir au menu avec `échap`. 

Il existe 2 modes de jeu distincts :
- Les **niveaux**, qui se succèdent et son définis par la configuration située dans `levels.json`.
- Le mode **infini**, qui génère simplement une *pomme* à un emplacement aléatoire sur la map, et qui se joue sans fin (il ne possède cependant pas de *murs*)

## Précisions

Quelques précisions :
- Tout le code est commenté de A à Z afin que vous ne soyiez pas perdue. 
- L'énoncé de M. Champin n'a pas été respecté à la lettre, nottament pour la partie `Représentation interne` avec le monde qui est censé être représenté dans un tableau bi-dimensionnel. Ceci est un **choix** car nous avons trouvé beaucoup plus simple sans cette manière de faire. De plus, d'autres éléments tel que le fichier de configuration des différents niveaux a été adapté pour nos besoins.

## Explication

Explication globale de comment fonctionne le code :
Tout d'abord, l'`index.html` va charger `main.ts`, qui va quant-à lui instancier le moteur de jeu (`src/core/Engine.ts`) et initiliaser le *canvas*, en mettant ces 2 éléments dans des variables globales pour qu'ils soient accessible de n'importe où dans le code.

Le jeu est divisé en *scènes* (retrouvables dans `src/scenes/*`), qui représentent les différentes interfaces/phases de jeu qui possèdent des logiques et des affichages différents. Ici nous en avons 2 : **Menu** (menu principal affiché au lancement de la page web) et **Game** (le jeu en lui même).

L'Engine va donc avoir ce rôle de gestionnaire des scènes. De plus, elle va stocker les variable d'état (*state*), de simples variables partagées entre les scènes (on y retrouve par exemple : le niveau choisi dans le menu, si le jeu est en pause ou non, etc).
Les classes des scènes possèdent quelques méthodes obligatoires :
- `awake()` -> appelée à la création de la scène
- `destroy()` -> appelée à la destruction de la scène (donc lors du chargement d'une autre scène)
- `loop()` -> boucle de jeu, appelée tous les X temps (X variable d'une scène à l'autre)
- `drawAll()` -> fonction de rendu de l'état du jeu grâce à l'API *canvas*
- `setEvents()` -> appelée au moment de la création de la scène, elle va permettre de définir les eventListeners utilisés par la scène

Chaque scène gère ensuite sa logique et ses attributs locaux comme elle le souhaite.

Enfin, les entitiés (`src/entities/*`) représentent les différents éléments présents dans le jeu, tel que le serpent, les murs, la pomme, etc.

## Structure du code

```
snake 
├── index.html
├── main.ts # point d'entrée de l'application
├── styles.scss # quelques styles de base 
├── package.json # configuration de node.js et des dépendances npm
├── public # dossier public, contenant les éléments statiques du site
│   └─ assets # images, videos, sons, svg, etc (dans notre cas, seulement des .mp3 sont contenus dedans)
├── config # contient les fichiers de configuration du jeu
│   ├─ game.json # paramètres de base de la partie
│   ├─ levels.json # configuration des différents niveaux
│   └─ world.json # configuration du monde et de son affichage
└─ src
    ├─ core # classe(s) servant de coeur à l'application, on y retrouve l'Engine (moteur de jeu)
    ├─ entities # les différentes entités du jeu
    ├─ scenes # les différentes scènes du jeu
    ├─ utils # fonctions utilitaires
    └─ types # qui dit TypeScript dit définition de types !
```

## Todo

- [x] Jeu de base
- [x] Score
- [x] Graphismes
- [x] Pause
- [x] Son
- [x] Menu principal
    - [x] Sélection niveau
- [x] Rédiger le README
    - [x] Expliquer TypeScript et pourquoi son utilisation
    - [x] Que tout est bien commenté comme il faut
    - [x] Lister sous forme de checklist les fonctionnalités implémentées
    - [x] Installation et Utilisation 
- [x] Compléter les commentaires
- [x] Imaginer des niveaux pour le `levels.json`
